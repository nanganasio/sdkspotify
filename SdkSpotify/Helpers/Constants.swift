//
//  Constants.swift
//  SdkSpotify
//
//  Created by MacBook Pro on 12/03/20.
//  Copyright © 2020 Adrian Cervantes. All rights reserved.
//

import Foundation

class Constants {
    static let accessTokenKey: String = "accessTokenKey"
    static let url: String = "https://api.spotify.com/v1/"
    static let redirectUri: String = "spotify-ios-quick-start://spotify-login-callback"
    static let clientId: String = "bcd261eeda904cc29257a5e8c387abae"
    static var bearer: String {
        let accessToken = UserDefaults.standard.object(forKey: Constants.accessTokenKey) as? String
        return accessToken ?? ""
    }
}
