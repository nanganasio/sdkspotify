//
//  SceneDelegate.swift
//  SdkSpotify
//
//  Created by MacBook Pro on 12/03/20.
//  Copyright © 2020 Adrian Cervantes. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    let redirectURL = URL(string: Constants.redirectUri)!
    var accessToken = UserDefaults.standard.string(forKey: Constants.accessTokenKey) {
        didSet {
            UserDefaults.standard.set(accessToken, forKey: Constants.accessTokenKey)
        }
    }
    lazy var configuration = SPTConfiguration(clientID: Constants.clientId, redirectURL: redirectURL)
    lazy var appRemote: SPTAppRemote = {
        let appRemote = SPTAppRemote(configuration: configuration, logLevel: .debug)
        appRemote.connectionParameters.accessToken = accessToken
        appRemote.delegate = self
        return appRemote
    }()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: scene.coordinateSpace.bounds)
        window?.windowScene = scene
        window?.makeKeyAndVisible()
        
        let view = ViewController()
        let navigation = UINavigationController(rootViewController: view)
        window?.rootViewController = navigation
    }
    
    //MARK: Connect Spotify
    private func connect() {
        appRemote.connect()
    }
    
    //MARK: Config Spotify Access Token
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else { return }
        let parameters = appRemote.authorizationParameters(from: url)
        if let access_token = parameters?[SPTAppRemoteAccessTokenKey] {
            appRemote.connectionParameters.accessToken = access_token
            accessToken = access_token
        } else if let error = parameters?[SPTAppRemoteErrorDescriptionKey] {
            print(error)
        }
    }
    
    //MARK: LifeCicle
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        if let _ = appRemote.connectionParameters.accessToken {
            appRemote.connect()
        }
    }

    func sceneWillResignActive(_ scene: UIScene) {
        if self.appRemote.isConnected {
            appRemote.disconnect()
        }
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}


//MARK: Remote Delegate
extension SceneDelegate: SPTAppRemoteDelegate {
    func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        print("CONNECTED")
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        print("FAIL IN CONNECTION")
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        print("DISCONNECT WITH ERROR")
    }
}
